import React from 'react'
import {Container, Row, Col} from 'react-bootstrap'

export default function Kpr() {
  return (
    <div className="kpr" >
    <Container fluid id="kpr">
      <Row>
        <Col>
          <div className="kprMain-1" style={{ paddingTop: "24px"}}>
              <img src="/kprMain-1.png" width="546px" />
          </div>
        </Col>
        <Col className="my-auto">
          <h3 className="h3-kpr">
            Kemudahan <br/> <span>Pengajuan KPR</span>{" "}
          </h3>
          <Row>
            <Col>
              <p>Kamu bisa melakukan pengajuan KPR secara online tanpa ribet. Proses pengajuan KPR ini bisa dilakukan dimana saja dengan melampirkan beberapa berkas pendukung sebagai syarat yang tersedia di aplikasi. Permudah kehidupanmu dengan Zenia.</p>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row style={{marginTop: "120px"}} >
        <Col>
        <h3 style={{textAlign: "center"}}>
          4 Langkah Mudah <span>Memiliki Hunian</span>{" "}
        </h3>
        </Col>
        </Row>
        <Row className="justify-content-md-center" style={{marginTop: "56px"}}>
          
          <Col md="auto">
          <div className="kpr-1">
          <img src="/kpr-1.png" width="261px" />
          </div>
          <div style={{textAlign: "center"}}>
            <h4>Mengisi<br/>From Pengajuan KPR</h4>
            <h6>Ajukan KPR secara online<br/>kapanpun tanpa ribet</h6>
          </div>
          </Col>
     
          
          <Col md="auto">
          <div className="kpr-1">
          <img src="/kpr-2.png" width="261px"/>
          </div>
          <div style={{textAlign: "center"}}>
            <h4>Mengikuti<br/>Proses Wawancara</h4>
            <h6>Proses wawancara dapat<br/>dilakukan secara online</h6>
            </div>
          </Col>
        
       
          <Col md="auto">
          <div className="kpr-1">
          <img src="/kpr-3.png" width="261px" />
          </div>
          <div style={{textAlign: "center"}}>
            <h4>Proses DP dan <br/> Pembayaran Cicilan</h4>
            <h6>Bayar DP minimal 15% dan mulai<br/>bayar cicilan KPR-mu</h6>
            </div>
          </Col>

          <Col md="auto">
          <div className="kpr-1">
          <img src="/kpr-4.png" width="261px" />
          </div>
          <div style={{textAlign: "center"}}>
            <h4>Miliki <br/>Hunian Impian Kamu</h4>
            <h6>Selesaikan cicilan dan miliki<br/>hunian impian kamu</h6>
            </div>
          </Col>
         
      </Row>
    </Container>
    </div>
  )
}
