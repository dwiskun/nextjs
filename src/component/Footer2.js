import React from 'react'
import {Container, Row, Col} from 'react-bootstrap'

export default function Footer2() {
  return (
    <Container fluid className="footer2">
        <Row className="justify-content-md-center">
        <Col md="auto">  <img src="/icon-footer.png" /> </Col>
        <Col md="auto" style={{paddingLeft:"0px"}}><p>2020 PT BINAR BANK INDONESIA</p></Col>
        </Row>
    </Container>
  )
}
