import React from 'react'
import Carousel from 'react-bootstrap/Carousel'
export default function BannerSlider() {
  return (
    <div>
      <Carousel>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src="/banner-1.png"
      alt="First slide"
    />
    <Carousel.Caption>
      <h3>Rencanakan dan Miliki Hunian Ternyaman Sejak Muda</h3>
      <p>Awali dengan menabung untuk mewujudkan hunian impian</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src="/banner-2.png"
      alt="Third slide"
    />

    <Carousel.Caption>
      <h3>Rencanakan dan Miliki Hunian Ternyaman Sejak Muda</h3>
      <p>Awali dengan menabung untuk mewujudkan hunian impian</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src="/banner-3.png"
      alt="Third slide"
    />

    <Carousel.Caption>
    <h3>Rencanakan dan Miliki Hunian Ternyaman Sejak Muda</h3>
      <p>Awali dengan menabung untuk mewujudkan hunian impian</p>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
    </div>
  )
}
