import React from 'react'
import {Container, Row, Col} from 'react-bootstrap'

export default function Footer() {
  return (
    <Container fluid className="footer" id="footer">
      <Row className="justify-content-md-center">
        <Col md="auto">
          <Row className="leftFooter1">
            <img src="/footer-1.png" width="356px"  />
          </Row>
          <Row className="leftFooter2">
            <p>Zenia adalah aplikasi mobile banking yang membantu milenial untuk merencanakan kepemilikan hunian impian tanpa ribet</p>
          </Row>
        </Col>
        <Col md="auto" style={{marginLeft:"1.75695461201vw"}}>
          <Row className="centerFooter1">
            <p>Hubungi Kami</p>
          </Row>
          <Row className="centerFooter2">
            <p>Jalan Tompeyan <br/>CT VIII Tegal Rejo, Yogyakarta<br/>DI Yogyakarta</p>
          </Row>
          <Row className="centerFooter3">
            <p>+62 85223558417</p>
          </Row>
          <Row className="centerFooter4">
            <p>support.zeniabinar@binar.com</p>
          </Row>
        </Col>
        <Col md="auto" style={{marginLeft:"8.71156661786vw"}}>
          <Row className="rightFooter1">
            <p>Follow Sosial Media Kami</p>
          </Row>
          <Row className="rightFooter2">
            <img src="/fbFooter.png" width="40px"/>
            <img src="/igFooter.png" width="40px"/>
            <img src="/twitterFooter.png" width="40px"/>
          </Row>
          <Row className="rightFooter3">
          <img src="/googleplay-1.png" width="110px"/>
          </Row>
        </Col>
      </Row>
    </Container>
  )
}
