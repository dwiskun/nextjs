import React from "react";
import { Container, Navbar, Nav } from "react-bootstrap";

function navbar() {
  return (
    
      <Navbar bg="light" expand="lg" sticky="top" >
        <Navbar.Brand href="/">
          <img src="/logo-zenia.png" width="105px" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="ml-auto" defaultActiveKey="/">
            <Nav.Link className="mr-3 " href="/">
              Beranda
            </Nav.Link>
            <Nav.Link className="mr-3" href="#kalkulator" eventKey="link-1">
              Kalkulator
            </Nav.Link>
            <Nav.Link className="mr-3" href="#tabungan" eventKey="link-2">
              Tabungan
            </Nav.Link>
            <Nav.Link className="mr-3" href="#kpr" eventKey="link-3">
              KPR
            </Nav.Link>
            <Nav.Link className="mr-3" href="#download" eventKey="link-4">
              Download
            </Nav.Link>
            <Nav.Link href="#footer" eventKey="link-5">
              Tentang
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    
  );
}

export default navbar;
