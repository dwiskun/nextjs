import React from 'react'
import {Container, Row, Col} from 'react-bootstrap'

export default function Download() {
  return (
    <div className="download" id="download">
      <Container fluid>
        <Row className="justify-content-md-center">
          <Col className="leftContent">
            <Row > 
              <h3 className="h3-kpr" >
                We Help You<span>Make Your Dream<br/> Come Ture</span>{" "}
              </h3>
            </Row>
            <Row style={{marginTop:"35px"}}>
              <p>
              Lengkapi kehidupanmu dan wujudkan mimpimu bersama kami. Zenia akan membantumu merencakanan impianmu untuk memiliki rumah dari muda
              </p>
            </Row> 
            <Row style={{marginTop:"42px"}} >
              <h2>
                Download Sekarang
              </h2>
            </Row>
            <Row >
              <img src="/googleplay-1.png" width="192px" style={{paddingTop:"24px"}} />
            </Row>  
          </Col>
          <Col className="rightContent">
            <img src="/download-1.png" width="452.8px" />
          </Col>
        </Row>
      </Container>
    </div>
  )
}
