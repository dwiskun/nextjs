import React from "react";
import { Container, Row, Col } from "react-bootstrap";

function tabungan() {
  return (
    <div className="tabungan" >
    <Container fluid className="container-fluid tabungan" style={{paddingLeft: "0",paddingRight: "0"}}>
    <section
      id="tabungan"
      className="tabungan-section"
      style={{
        backgroundImage: "url(" + `${require("../../public/tab-bg.png")}` + ")",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        height:"800px"
      }}
    >
      
        <Row className="justify-content-md-center" style={{marginLeft: "9.15080527vw" }}  >
          <Col md="auto">
            <h3>
              Tabungan <span>DP Hunian</span>{" "}
            </h3>
            <Row className="pt-5" >
              <Col md="auto">
                <img src="/tab-1.png" width="70px" />
              </Col>
              <Col>
                <h5>Hitung Kemampuan</h5>
                <p>
                  Awali dari hasil perhitungan kalkulator untuk memperoleh
                  rekomendasi perencanaan yang tepat
                </p>
              </Col>
            </Row>
            <Row className="pt-3">
              <Col md="auto">
                <img src="/tab-2.png" width="70px" />
              </Col>
              <Col>
                <h5>Rekomendasi Tabungan</h5>
                <p>
                  Rekomendasi tabungan DP hunian dengan template yang sudah
                  tersedia atau custom sesuai dengan kebutuhan kamu
                </p>
              </Col>
            </Row>
            <Row className="pt-3">
              <Col md="auto">
                <img src="/tab-3.png" width="70px" />
              </Col>
              <Col>
                <h5>Setoran Awal Fleksibel</h5>
                <p>Setoran awal dapat kamu sesuaikan dengan kemampuan kamu</p>
              </Col>
            </Row>
          </Col>
          <Col className="tabungan-image">
          
              <img src="/tabungan.png" width="600px" />
   
          </Col>
        </Row>
        
    </section>
    </Container>
    </div>
  );
}

export default tabungan;
