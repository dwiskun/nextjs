import React from 'react'
import {Container, Row, Col} from 'react-bootstrap'

export default function ServiceProgram() {
  return (
    <div className="serviceProgram">
    <Container fluid>
      <Row style={{paddingBottom:"64px"}} >
        <Col>
        <h3 style={{textAlign: "center"}}>
          Pelayanan Terbaik <span>Dari Kami</span>{" "}
        </h3>
        </Col>
        </Row>
        <Row className="justify-content-md-center">
          
          <Col md="auto" >
          <div className="sp-1">
          <img src="/sp-1.png" width="356px" />
          </div>
          <div style={{textAlign: "center"}}>
            <h4>KALKULATOR</h4>
            <h6>Hitung kemampuan<br/>menabung/cicilan, setoran tabungan<br/>DP, dan cicilan KPR setiap bulan</h6>
          </div>
          </Col>
     
          
          <Col md="auto">
          <div className="sp-1">
          <img src="/sp-1.png" width="356px"/>
          </div>
          <div style={{textAlign: "center"}}>
            <h4>TABUNGAN DAN KPR</h4>
            <h6>Kemudian menabung DP dan<br/>pengahuan KPR dalam satu aplikasi</h6>
            </div>
          </Col>
        
       
          <Col md="auto">
          <div className="sp-1">
          <img src="/sp-1.png" width="356px" />
          </div>
          <div style={{textAlign: "center"}}>
            <h4>KEAMANAN</h4>
            <h6>Layanan pengaduan 24 jam</h6>
            </div>
          </Col>
         
      </Row>
      </Container>
      </div>
  )
}
