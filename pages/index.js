import Head from "next/head";


import Navbar from "../src/component/Navbar";
import Tabungan from "../src/component/Tabungan";
import BannerSlider from "../src/component/BannerSlider";
import ServiceProgram from "../src/component/ServiceProgram";
import Kpr from "../src/component/Kpr";
import Download from "../src/component/Download";
import Footer from "../src/component/Footer";
import Footer2 from "../src/component/Footer2";

const Home = () => (
  <>
    <Head>
      <title>Create Next App</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>
    
    <Navbar />
    <BannerSlider/>
    <ServiceProgram/>
    <Tabungan />
    <Kpr/>
    <Download/>
    <Footer/>
    <Footer2/>
  </>
);

export default Home;
